class MessagesController < ApplicationController

  # Create a sha256 hash digest for the received string
  def create
    sha256 = Digest::SHA256.hexdigest params[:message]
    # Save the hash digest and string in the Database
    Message.find_or_create_by(hash_digest: sha256, message: params[:message])
    render json: {
      digest: sha256,
    }.to_json, status: 200
  end

  # return string for requested hash digest if available
  def show
    # Retrieve hash digest from table Message
    sha256 = Message.find_by(hash_digest: params[:hash_digest])
    if sha256
      msg = sha256.message
      render json: {
        message: msg
      }.to_json, status: 200
    else
      render json: {
        err_msg: "message not found"
      }.to_json, status: 404
    end
  end

end
