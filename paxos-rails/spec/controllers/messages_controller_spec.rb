require 'rails_helper'
require 'rspec-rails'

describe MessagesController, type: :request do
  let(:hash_val) {'2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824'}
  describe 'check post request' do
    it 'responds with hash digest and status 200' do
      msg = { message: "hello" }
      post '/messages', params: msg.to_json, headers: { 'Content-Type': 'application/vnd.api+json' }
      expect(response.status).to eq 200
      expect(json['digest']).to eq(hash_val)
    end
  end

  describe 'check get request' do
    it 'responds with message and status 200' do
      FactoryGirl.create :message, hash_digest: hash_val, message: 'hello'
      get "/messages/#{hash_val}"
      expect(response.status).to eq 200
      expect(json['message']).to eq("hello")
    end

    it 'responds with status 404' do
      get "/messages/#{hash_val}"
      expect(response.status).to eq 404
      expect(json['err_msg']).to eq("message not found")
    end
  end
end
