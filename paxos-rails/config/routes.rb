Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #resources :messages
  post '/messages', to: 'messages#create'
  get '/messages/:hash_digest', to: 'messages#show'
end
