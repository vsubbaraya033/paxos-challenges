#!/usr/bin/env python3
import sys
import queue

def gen_pattern(str):
    # Create a queue
    q = queue.Queue()
    q.put(str)

    while not q.empty():
        tmp = q.get()
        # check for first appearance of x
        index = tmp.find('x')
        if(index != -1):
            # Replace x with 0 and 1 using string manipulation and add to queue
            tmp = list(tmp)
            tmp[index] = '0'
            q.put("".join(tmp))
            tmp[index] = '1'
            tmp = "".join(tmp)
            q.put(tmp)
        else:
            # print the binary string
            print(tmp)


str = sys.argv[1]
gen_pattern(str.lower())
