#!/usr/bin/env python3
import sys

# Find the 2 items that is equal or less than given sum
def calculation():
    arr_len = len(prices)
    i=0
    j = arr_len - 1
    
    # Check for base cases
    if(arr_len <= 1):
        return 0
    if(prices[j] + prices[j-1] < sum):
        return j,j-1
    if(prices[i] + prices[i+1] > sum):
        return 0
        
    min_diff = sys.maxsize
    # Iterate over the prices array using two pointers
    while i<j:
        curr_sum = prices[i] + prices[j]
        diff = curr_sum - sum
        if(diff == 0):
            return i,j
        #check if minimum difference is found
        if(diff < 0 and abs(diff) < min_diff):
            min_diff = abs(diff)
            index1 = i
            index2 = j
        # Increment or decrement the pointers
        if (diff < 0):
            i += 1
        else:
            j -= 1
    return index1, index2

#Print the final result( using 2 items)
def print_result(vals):
    if(vals):
        print("%s %d, %s %d" %(items[vals[0]], prices[vals[0]],items[vals[1]], prices[vals[1]]))
    else:
        print("Not possible")

# Parse the input by making two separate arrays for prices and items
def parse_input(ip_file):
    with open(ip_file, "r") as filer:
        for line in filer.readlines():
            temp = line.rstrip("\n").split(",")
            try:
                items.append(temp[0])
                prices.append(int(temp[1]))
            except: pass


# Get all the subsets of prices array
def get_subsets(nums):
  if nums is None: return None
  subsets = [[]] 
  next = [] 
  for n in nums:
    for s in subsets:
      next.append(s + [n])
    subsets += next
    next = []
  return subsets

# Find one or more items that is equal or less than target sum
def calculate_moregifts(subsets):
    min_diff = sys.maxsize
    result = 0
    # Base case
    if prices[0] > sum:
        return 0
    # loop through each subset
    for i in range(1, len(subsets)):
        arr = subsets[i]
        curr_sum = 0
        # find sum of current array
        for var in arr:
            curr_sum += var

        diff = curr_sum - sum
        if(diff == 0):
            result = arr
        # check for minimum difference
        if(diff < 0 and abs(diff) < min_diff):
            min_diff = abs(diff)
            result = arr

    return result

# Print the result using hash (for 1 or more items)
def getmoreGifts(vals):
    if(vals and len(vals) > 0):
        dict = {}
        for i in range(len(items)):
            dict[items[i]] = prices[i]
        keys = []
        for res in vals:
            for key,value in dict.items():
                if(res == value  and res not in keys):
                    print('%s %d' %(key,value))
                    keys.append(key)
    else:
        print("Not possible")
        

# Get the target sum from the argument passed
sum = int(sys.argv[2])
items = []
prices = []
parse_input(sys.argv[1])

print("----- Using 2 items -----")
vals = calculation()
print_result(vals)
print("")

print("----- Using 1 or more items -----")
subsets = get_subsets(prices)
vals = calculate_moregifts(subsets) 
getmoreGifts(vals)
